package com.automata.testing.framework.post.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.automata.testing.framework.post.model.PostEntity;
import com.automata.testing.framework.post.repository.IPostRepository;

@Service
public class PostRepositoryTest implements IPostRepository {
	Map<Integer, PostEntity> map = new HashMap<>();
	
	@Override
	public <S extends PostEntity> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public <S extends PostEntity> S save(S entity) {
		map.put(entity.getId(), entity);
		return entity;
	}
	
	@Override
	public Optional<PostEntity> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Iterable<PostEntity> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Iterable<PostEntity> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void deleteById(Integer id) {
		map.remove(id);
	}
	
	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void deleteAll(Iterable<? extends PostEntity> entities) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void delete(PostEntity entity) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public long count() {
		return map.size();
	}
}
