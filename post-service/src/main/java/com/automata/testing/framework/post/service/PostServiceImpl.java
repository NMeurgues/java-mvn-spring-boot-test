package com.automata.testing.framework.post.service;
/*
 * Copyright: Copyright (c) Automata akt.io 2022
 */

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.automata.testing.framework.algorithm.service.IDecryptionService;
import com.automata.testing.framework.algorithm.service.IEncryptionService;
import com.automata.testing.framework.post.dto.PostDTO;
import com.automata.testing.framework.post.model.PostEntity;
import com.automata.testing.framework.post.repository.IPostRepository;

/**
 * Dependencies
 */

/**
 * Post service implementation.
 * 
 * @author GELIBERT
 */
public class PostServiceImpl implements IPostService {

    // -------------------------------------- Inner classes

    // -------------------------------------- public static attributes

    // -------------------------------------- private static attributes

    // -------------------------------------- private attributes

    /**
     * Loading the post repository in order to load datas.
     */
    @Autowired
    private IPostRepository postRepo;
    
    @Autowired
    private IEncryptionService encryptionService;
    
    @Autowired
    private IDecryptionService decryptionService;

    // -------------------------------------- public attributes

    // -------------------------------------- Constructor

    // -------------------------------------- Public static methods

    // -------------------------------------- Private static methods

    // -------------------------------------- Private methods

    // -------------------------------------- Protected methods

    // -------------------------------------- Public methods

    /**
     * {@inheritDoc}
     */
    public Integer createPost(PostDTO post) {
		PostEntity postDoc = PostEntity.builder()
				.content(encryptionService.encode(post.getContent()))
				.userId(encryptionService.encode(post.getUser().getUserId()))
				.userFirstName(encryptionService.encode(post.getUser().getFirstName()))
				.userLastName(encryptionService.encode(post.getUser().getLastName()))
				.build();
		
		return postRepo.save(postDoc).getId();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<PostEntity> getPost(Integer id) {
    	return postRepo.findById(id).flatMap((PostEntity element) -> {
    		element.setContent(decryptionService.decode(element.getContent()));
    		element.setUserId(decryptionService.decode(element.getUserId()));
    		element.setUserFirstName(decryptionService.decode(element.getUserFirstName()));
    		element.setUserLastName(decryptionService.decode(element.getUserLastName()));
    		return Optional.of(element);
    	});
    }
    
    @Override
    public void deletePost(Integer id) {
    	postRepo.deleteById(id);
    }

    // -------------------------------------- Setters and Getters

}
