package com.automata.testing.framework.commons.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.automata.testing.framework.commons.error.ApiError;

@ControllerAdvice
public class CommonControllerAdvice {
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ApiError> handleCityNotFoundException(HttpMessageNotReadableException ex, WebRequest request) {
		final String error = "Malformed JSON request";
		final String code = "GENERAL_001";
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error, code, ex);
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }
}
